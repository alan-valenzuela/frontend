import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import axios from 'axios';

 export default class PostList extends Component {

    state = {
        posts : [],
        newPost: ""
    }

   async componentDidMount () {
        const res = await axios.get('https://jsonplaceholder.typicode.com/posts');
        this.setState({posts : res.data});
    }

    render() {
         return (
          <ul className = "list-group">
            {this.state.posts.map(post => 
                <li className="list-group-item" key={post.id} to="/">
                    {post.body}
                </li>
            )}      
          </ul>
         )
     }
 }