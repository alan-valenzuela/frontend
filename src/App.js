import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/Navbar';
import CreatePost from './components/CreatePost';
import PostsList from './components/PostList';
import { BrowserRouter as Router, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Navbar />
      <Route path="/postslist" exact component={PostsList} />
      <Route path="/createpost" exact component={CreatePost} />
    </Router>
  );
}

export default App;
